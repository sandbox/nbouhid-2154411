<?php
/**
 * @file
 * This file contains all the needed to manage the admin area.
 */

/**
 * Generates the admin form to configure the auto_captcha module.
 *
 * @return array
 *   The form array with fields.
 */
function auto_captcha_admin_settings() {
  // This will make a difference on the configuration.
  $webform_installed = module_exists('webform');
  $captcha_challenges = _auto_captcha_get_all_captcha_types();

  $form['auto_captcha_informative_text'] = array(
    '#title' => t(''),
    '#markup' => t('!topenHow to configure it!tclose!popenSelect a challenge. It will only apply if the form isn\'t configured on the captcha settings page.!pclose',
      array(
        '!topen' => '<h2>',
        '!tclose' => '</h2>',
        '!popen' => '<p>',
        '!pclose' => '</p>'
      )
    ),
  );

  $form['auto_captcha_default_challenge_system'] = array(
    '#type' => 'select',
    '#title' => t('Default challenge type for all forms'),
    '#options' => $captcha_challenges,
    '#default_value' => variable_get('auto_captcha_default_challenge_system'),
  );
  $system_form_title = t('Default challenge type for system forms (all except webforms)');
  if ($webform_installed) {
    $form['auto_captcha_default_challenge_system']['#title'] = $system_form_title;

    $form['auto_captcha_default_challenge_webform'] = array(
      '#type' => 'select',
      '#title' => t('Default challenge type for webforms'),
      '#options' => $captcha_challenges,
      '#default_value' => variable_get('auto_captcha_default_challenge_webform'),
    );
  }

  // Submit button.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
 * Submission function for auto_captcha_admin_settings form.
 */
function auto_captcha_admin_settings_submit($form, &$form_state) {
  variable_set('auto_captcha_default_challenge_system', $form_state['values']['auto_captcha_default_challenge_system']);
  if ($form_state['values']['auto_captcha_default_challenge_webform']) {
    variable_set('auto_captcha_default_challenge_webform', $form_state['values']['auto_captcha_default_challenge_webform']);
  }

  drupal_set_message(t('The auto CAPTCHA settings have been saved.'), 'status');
}

/**
 * Invokes the captcha module to retrieve all captcha types.
 *
 * @return array
 *   See captcha.admin.inc
 */
function _auto_captcha_get_all_captcha_types() {
  module_load_include('inc', 'captcha', 'captcha.admin');
  return _captcha_available_challenge_types();
}
